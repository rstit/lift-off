class FrontendMailer < ActionMailer::Base
  default from: "system@kmpit.com"
  
  def hire_us_form(to, from, data)
    @data = data
    mail(to: to, from: from, :subject => "KMPIT - Hire Us")
  end
  
end
