class Service < ActiveRecord::Base
  
  has_one :contact_requests, dependent: :nullify
  
end
