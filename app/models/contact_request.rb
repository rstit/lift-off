class ContactRequest < ActiveRecord::Base
  
  validates :name, :telephone, :email, presence: true
  validates :email, format: /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/
  validates :description, presence: true, length: { maximum: 240 }

end
