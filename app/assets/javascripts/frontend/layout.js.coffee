window.Header = {}
window.Map = {}

window.$.fn.setSameHeight = (class_name) ->
  wrapper = $(this)
  h = 0
  wrapper.find(class_name).each ->
    if h < $(this).height()
      h = $(this).height()
    return h
  wrapper.find(class_name).height(h)
    
window.$.fn.setVerticalAlign = (class_name) ->
  $(this).each ->
    wrapper = $(this)
    h = wrapper.height()
    wrapper.find(class_name).each ->
      m = (h - $(this).height())/2
      $(this).css({marginTop: m, marginBottom: m})

Header.styckyOn = () ->
  $('#top_bar').addClass 'stycky'
  
Header.styckyOff = () ->
  $('#top_bar').removeClass 'stycky'
  
Map.initialize = (map_id, lat, lng, zoom, map_type) ->
  myOptions =
    zoom: zoom
    center: new google.maps.LatLng(lat, lng)
    mapTypeId: Map.setMapType(map_type)
    overviewMapControl: false
    scaleControl: false
    streetViewControl: false
    mapTypeControl: false
    panControl: false
    rotateControl: false
    zoomControl: true
    zoomControlOptions:
      style: google.maps.ZoomControlStyle.SMALL
    scrollwheel: false

  map = new google.maps.Map(document.getElementById(map_id), myOptions)
  map

Map.setMapType = (map_type) ->
  switch map_type
    when "ROADMAP"
      return google.maps.MapTypeId.ROADMAP
    when "TERRAIN"
      return google.maps.MapTypeId.TERRAIN
    when "HYBRID"
      return google.maps.MapTypeId.HYBRID
    when "SATELLITE"
      return google.maps.MapTypeId.SATELLITE

Map.addMarker = (map, pos, title) ->
  marker = new google.maps.Marker(
    position: pos
    map: map
    title: title
  )
  marker

Map.addInfoWindow = (map, marker, description) ->
  infowindow = new google.maps.InfoWindow(content: description)
  google.maps.event.addListener marker, "click", ->
    infowindow.open map, marker
    
$ ->
  $(document).foundation()
  
  $(window).load ->
    $('#what_we_do .blocks').setSameHeight('.section-heading h3')
    $('#technologies .blocks').setSameHeight('.section-heading h3')
    $('#selected_tools ul').setVerticalAlign("img")
    $('#selected_technologies ul').setVerticalAlign("img")
  
  $(window).resize ->
    $('#selected_tools ul').setVerticalAlign("img")
  
  $(window).scroll (e) ->
    st = $(this).scrollTop()
    $('#main_menu li').each ->
      id = $(this).find('a.to-section').attr('href')
      if $(id).length == 1
        offset = parseInt($(id).offset().top) - 65
        if offset <= st
          $('#main_menu').find('.selected').removeClass('selected')
          $(this).addClass('selected')
          $('#content').find('section.active').removeClass('active')
          $(id).addClass('active')
      
  $('.to-section').click (e) ->
    that = this
    $('html, body').stop().animate({'scrollTop': $($(this).attr('href')).offset().top-65},{duration: 800, complete: -> 
      if $(that).parents('#main_menu').length > 0
        $('#main_menu .selected').removeClass('selected')
        $(that).parents('li').addClass('selected')
    })
    e.preventDefault()
    false
  
  map = Map.initialize("googlemap", 51.82711315247187, 0, 4, "ROADMAP")

  # Köln
  pos = new google.maps.LatLng(50.941782, 6.943526700000007)
  title = "Köln"
  marker = Map.addMarker(map, pos, title)
  newImage = new google.maps.MarkerImage("/markerrst.png", new google.maps.Size(69, 75), new google.maps.Point(0, 0), new google.maps.Point(35, 75), new google.maps.Size(69, 75))
  marker.setIcon(null)
  marker.setIcon(newImage)

  # Wrocław
  pos = new google.maps.LatLng(51.1103337, 16.973388800000066)
  title = "Wrocław"
  marker = Map.addMarker(map, pos, title)
  newImage = new google.maps.MarkerImage("/markerrst.png", new google.maps.Size(69, 75), new google.maps.Point(0, 0), new google.maps.Point(35, 75), new google.maps.Size(69, 75))
  marker.setIcon(null)
  marker.setIcon(newImage)
 
     