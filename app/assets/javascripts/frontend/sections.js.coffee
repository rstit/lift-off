window.References = {}
window.Projects = {}

References.active = (index) ->
  wrapper = $('#references .references-wrapper')
  currentRef = wrapper.find(".reference.active")
  nextRef = wrapper.find(".reference:eq(#{index})")
  
  wrapper.css({height: currentRef.outerHeight()})
  nextRef.addClass('animation-start')
  currentRef.removeClass('active animation-end')
  nextRef.addClass('active')
  wrapper.stop().animate({height: nextRef.outerHeight()})
  nextRef.removeClass('animation-start').addClass('animation-end')
  
References.next = ->
  n = $('#references .reference').length
  wrapper = $('#references .references-wrapper')
  currentRef = wrapper.find(".reference.active")
  index = currentRef.index()
  if index < n-1
    References.active(index+1)
  else
    References.active(0)
    
References.prev = ->
  n = $('#references .reference').length
  wrapper = $('#references .references-wrapper')
  currentRef = wrapper.find(".reference.active")
  index = currentRef.index()
  if index > 0
    References.active(index-1)
  else
    References.active(n-1)
    
    
Projects.activeName = (index)->
  wrapper = $('#selected_projects .project-name')
  wrapper.find('.active').stop().animate({opacity: 0},{complete: -> $(this).hide().removeClass('active')})
  wrapper.find(".name:eq(#{index})").show().stop().animate({opacity: 1},{complete: -> $(this).addClass('active')})
  
Projects.activeTechnologies = (index)->
  wrapper = $('#selected_projects .project-technologies')
  wrapper.find('.active').stop().animate({opacity: 0},{complete: -> $(this).hide().removeClass('active')})
  wrapper.find(".technologies:eq(#{index})").show().stop().animate({opacity: 1},{complete: -> $(this).addClass('active')})
  
Projects.activeDescription = (index)->
  wrapper = $('#selected_projects .project-description')
  wrapper.find('.active').stop().animate({opacity: 0},{complete: -> $(this).hide().removeClass('active')})
  wrapper.find(".description:eq(#{index})").show().stop().animate({opacity: 1},{complete: -> $(this).addClass('active')})

# MGS 
Projects.mgsOutLeft = ( callback )->
  wrapper = $('#carousel .slide.mgs')
  wrapper.find('.layer1').stop().delay(0).animate({left: -1000, opacity: 0})
  wrapper.find('.layer2').stop().delay(100).animate({left: -1000, opacity: 0})
  wrapper.find('.layer3').stop().delay(200).animate({left: -1000, opacity: 0},{complete: -> callback()})
  
Projects.mgsOnLeft = ( callback )->
  layer1Left = -235
  layer2Left = -9
  layer3Left = 463
  wrapper = $('#carousel .slide.mgs')
  wrapper.find('.layer').css({left: 1600, opacity: 0})
  wrapper.find('.layer1').stop().delay(0).animate({left: layer1Left, opacity: 1})
  wrapper.find('.layer2').stop().delay(200).animate({left: layer2Left, opacity: 1})
  wrapper.find('.layer3').stop().delay(200).animate({left: layer3Left, opacity: 1},{complete: -> callback()})
  
Projects.mgsOutRight = ( callback )->
  wrapper = $('#carousel .slide.mgs')
  wrapper.find('.layer1').stop().delay(200).animate({left: 1600, opacity: 0},{complete: -> callback()})
  wrapper.find('.layer2').stop().delay(100).animate({left: 1600, opacity: 0})
  wrapper.find('.layer3').stop().delay(0).animate({left: 1600, opacity: 0})
  
Projects.mgsOnRight = ( callback )->
  layer1Left = -235
  layer2Left = -9
  layer3Left = 463
  wrapper = $('#carousel .slide.mgs')
  wrapper.find('.layer').css({left: -1000, opacity: 0})
  wrapper.find('.layer1').stop().delay(200).animate({left: layer1Left, opacity: 1},{complete: -> callback()})
  wrapper.find('.layer2').stop().delay(100).animate({left: layer2Left, opacity: 1})
  wrapper.find('.layer3').stop().delay(0).animate({left: layer3Left, opacity: 1})
  
# Taxi5
Projects.taxi5OutLeft = ( callback )->
  wrapper = $('#carousel .slide.taxi5')
  wrapper.find('.layer1').stop().delay(0).animate({left: -1000, opacity: 0})
  wrapper.find('.layer2').stop().delay(100).animate({left: -1000, opacity: 0})
  wrapper.find('.layer3').stop().delay(200).animate({left: -1000, opacity: 0})
  wrapper.find('.layer4').stop().delay(400).animate({left: -1000, opacity: 0},{complete: -> callback()})
  
Projects.taxi5OnLeft = ( callback )->
  layer1Left = -172
  layer2Left = 85
  layer3Left = 544
  layer4Left = 710
  wrapper = $('#carousel .slide.taxi5')
  wrapper.find('.layer').css({left: 1600, opacity: 0})
  wrapper.find('.layer1').stop().delay(0).animate({left: layer1Left, opacity: 1})
  wrapper.find('.layer2').stop().delay(100).animate({left: layer2Left, opacity: 1})
  wrapper.find('.layer3').stop().delay(200).animate({left: layer3Left, opacity: 1})
  wrapper.find('.layer4').stop().delay(400).animate({left: layer4Left, opacity: 1},{complete: -> callback()})
  
Projects.taxi5OutRight = ( callback )->
  wrapper = $('#carousel .slide.taxi5')
  wrapper.find('.layer1').stop().delay(400).animate({left: 1600, opacity: 0},{complete: -> callback()})
  wrapper.find('.layer2').stop().delay(200).animate({left: 1600, opacity: 0})
  wrapper.find('.layer3').stop().delay(100).animate({left: 1600, opacity: 0})
  wrapper.find('.layer4').stop().delay(0).animate({left: 1600, opacity: 0}) 
  
Projects.taxi5OnRight = ( callback )->
  layer1Left = -172
  layer2Left = 85
  layer3Left = 544
  layer4Left = 710
  wrapper = $('#carousel .slide.taxi5')
  wrapper.find('.layer').css({left: -1000, opacity: 0})
  wrapper.find('.layer1').stop().delay(400).animate({left: layer1Left, opacity: 1},{complete: -> callback()})
  wrapper.find('.layer2').stop().delay(200).animate({left: layer2Left, opacity: 1})
  wrapper.find('.layer3').stop().delay(100).animate({left: layer3Left, opacity: 1})
  wrapper.find('.layer4').stop().delay(0).animate({left: layer4Left, opacity: 1})
  
# Mood
Projects.moodOutLeft = ( callback )->
  wrapper = $('#carousel .slide.mood')
  wrapper.find('.layer1').stop().delay(0).animate({left: -1000, opacity: 0})
  wrapper.find('.layer2').stop().delay(100).animate({left: -1000, opacity: 0})
  wrapper.find('.layer3').stop().delay(200).animate({left: -1000, opacity: 0},{complete: -> callback()})
  
Projects.moodOnLeft = ( callback )->
  layer1Left = -235
  layer2Left = 49
  layer3Left = 463
  wrapper = $('#carousel .slide.mood')
  wrapper.find('.layer').css({left: 1600, opacity: 0})
  wrapper.find('.layer1').stop().delay(0).animate({left: layer1Left, opacity: 1})
  wrapper.find('.layer2').stop().delay(100).animate({left: layer2Left, opacity: 1})
  wrapper.find('.layer3').stop().delay(200).animate({left: layer3Left, opacity: 1},{complete: -> callback()})
  
Projects.moodOutRight = ( callback )->
  wrapper = $('#carousel .slide.mood')
  wrapper.find('.layer1').stop().delay(200).animate({left: 1600, opacity: 0},{complete: -> callback()})
  wrapper.find('.layer2').stop().delay(100).animate({left: 1600, opacity: 0})
  wrapper.find('.layer3').stop().delay(0).animate({left: 1600, opacity: 0})

Projects.moodOnRight = ( callback )->
  layer1Left = -235
  layer2Left = 49
  layer3Left = 463
  wrapper = $('#carousel .slide.mood')
  wrapper.find('.layer').css({left: -1000, opacity: 0})
  wrapper.find('.layer1').stop().delay(200).animate({left: layer1Left, opacity: 1},{complete: -> callback()})
  wrapper.find('.layer2').stop().delay(100).animate({left: layer2Left, opacity: 1})
  wrapper.find('.layer3').stop().delay(0).animate({left: layer3Left, opacity: 1})
   
# Flicolo
Projects.flicoloOutLeft = ( callback )->
  wrapper = $('#carousel .slide.flicolo')
  wrapper.find('.layer1').stop().delay(0).animate({left: -1000, opacity: 0})
  wrapper.find('.layer2').stop().delay(100).animate({left: -1000, opacity: 0})
  wrapper.find('.layer3').stop().delay(200).animate({left: -1000, opacity: 0})
  wrapper.find('.layer4').stop().delay(300).animate({left: -1000, opacity: 0})
  wrapper.find('.layer5').stop().delay(700).animate({opacity: 0},{complete: -> callback()})
  
Projects.flicoloOnLeft = ( callback )->
  layer1Left = -176
  layer2Left = 249
  layer3Left = 390
  layer4Left = 585
  wrapper = $('#carousel .slide.flicolo')
  wrapper.find('.layer').not('.layer5').css({left: 1600, opacity: 0})
  wrapper.find('.layer5').stop().delay(0).animate({opacity: 1})
  wrapper.find('.layer1').stop().delay(100).animate({left: layer1Left, opacity: 1})
  wrapper.find('.layer2').stop().delay(200).animate({left: layer2Left, opacity: 1})
  wrapper.find('.layer3').stop().delay(300).animate({left: layer3Left, opacity: 1})
  wrapper.find('.layer4').stop().delay(400).animate({left: layer4Left, opacity: 1},{complete: -> callback()})

Projects.flicoloOutRight = ( callback )->
  wrapper = $('#carousel .slide.flicolo')
  wrapper.find('.layer1').stop().delay(300).animate({left: 1600, opacity: 0})
  wrapper.find('.layer2').stop().delay(200).animate({left: 1600, opacity: 0})
  wrapper.find('.layer3').stop().delay(100).animate({left: 1600, opacity: 0})
  wrapper.find('.layer4').stop().delay(0).animate({left: 1600, opacity: 0})
  wrapper.find('.layer5').stop().delay(700).animate({opacity: 0},{complete: -> callback()})
  
Projects.flicoloOnRight = ( callback )->
  layer1Left = -176
  layer2Left = 249
  layer3Left = 390
  layer4Left = 585
  wrapper = $('#carousel .slide.flicolo')
  wrapper.find('.layer').not('.layer5').css({left: -1000, opacity: 0})
  wrapper.find('.layer5').css({opacity: 1})
  wrapper.find('.layer1').stop().delay(300).animate({left: layer1Left, opacity: 1},{complete: -> callback()})
  wrapper.find('.layer2').stop().delay(200).animate({left: layer2Left, opacity: 1})
  wrapper.find('.layer3').stop().delay(100).animate({left: layer3Left, opacity: 1})
  wrapper.find('.layer4').stop().delay(0).animate({left: layer4Left, opacity: 1})

  
  
  
Projects.activeSlide = (index, dir)->
  if dir == "next"
    switch index
      when 1 then Projects.mgsOutLeft( -> Projects.taxi5OnLeft( -> Projects.setActiveSlide(index)))
      when 2 then Projects.taxi5OutLeft( -> Projects.moodOnLeft( -> Projects.setActiveSlide(index)))
      when 3 then Projects.moodOutLeft( -> Projects.flicoloOnLeft( -> Projects.setActiveSlide(index)))
      when 0 then Projects.flicoloOutLeft( -> Projects.mgsOnLeft( -> Projects.setActiveSlide(index)))
  if dir == "prev"
    switch index
      when 3 then Projects.mgsOutRight( -> Projects.flicoloOnRight( -> Projects.setActiveSlide(index)))
      when 2 then Projects.flicoloOutRight( -> Projects.moodOnRight( -> Projects.setActiveSlide(index)))
      when 1 then Projects.moodOutRight( -> Projects.taxi5OnRight( -> Projects.setActiveSlide(index)))
      when 0 then Projects.taxi5OutRight( -> Projects.mgsOnRight( -> Projects.setActiveSlide(index)))
    
Projects.setActiveSlide = (index)->
  wrapper = $('#carousel')
  wrapper.find('.active').removeClass('active')
  wrapper.find(".slide:eq(#{index})").addClass('active')
  
Projects.activeProject = (index, dir)->
  Projects.activeName(index)
  Projects.activeDescription(index)
  Projects.activeTechnologies(index)
  Projects.activeSlide(index, dir)
  

Projects.prev = ->
  n = $('#selected_projects .slide').length
  wrapper = $('#carousel')
  currentProj = wrapper.find(".slide.active")
  index = currentProj.index()
  if index > 0
    Projects.activeProject(index-1, "prev")
  else
    Projects.activeProject(n-1, "prev")
    
Projects.next = ->
  n = $('#selected_projects .slide').length
  wrapper = $('#carousel')
  currentProj = wrapper.find(".slide.active")
  index = currentProj.index()
  if index < n-1
    Projects.activeProject(index+1, "next")
  else
    Projects.activeProject(0, "next")
   
$ ->
  $('#references .prev').click -> References.prev()
  $('#references .next').click -> References.next()
  
  $('#selected_projects .prev').click -> Projects.prev()
  $('#selected_projects .next').click -> Projects.next()
  
  $(':radio').iCheck({radioClass: 'iradio iradio_flat-red'})
  $(':checkbox').iCheck({checkboxClass: 'icheck' })