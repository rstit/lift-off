module FrontendHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end
  
  def main_menu
    %w( home what_we_do our_team blog hire_us contact )
  end
  
  def section_heading section
    html = <<-HTML
      <header class="section-heading">
        <h2>
          #{I18n.t(section, scope: "section_headings")}
          <span class="border"></span>
        </h2>
      </header>
    HTML
    html.html_safe
  end
  
  def technologies
    [
      ["HTML5 / HAML / SLIM","CSS3 / SASS / SCSS","JavaScript","CoffeeScript", "AngularJS", "Backbone.js", "Marionette.js","WEBGL"],
      ["Ruby on Rails", "Node.js", "Express.js", "Websockets", "Objective C","Java"],
      ["REST API","OAuth2","JSON / XML","Google Maps API","Facebook API","Twitter API","Open ID"],
      ["MySQL","PostgreSQL","Amazon (AWS)","NGINX","MongoDB","Prediction.io", "ElasticSearch" ,"Redis","Memcached"]
    ]
  end
  
  def parse_to_img_url text
    text.downcase.gsub(" ","-")
  end
  
  
  # collections used in sections
  
  def web_and_mobile_collection # home section
    [
      "AngularJS",
      "Ruby on Rails",
      "Android",
      "iOS",
      "HTML5",
      "RWD"
    ]
  end
  
  def references_collection # references section
    [
      {name: "Grzegorz Kornak", position: "Director of Infrastructure Division", company: "Asseco Poland", text: t("ref1", scope: "references"), img_url: "/references/01.png"},
      {name: "Katarzyna Dorsey", position: "President at", company: "Yosh", text: t("ref2", scope: "references"), img_url: "/references/02.png"},
      {name: "Małgorzat Stuła", position: "CEO", company: "My Green Space", text: t("ref3", scope: "references"), img_url: "/references/03.png"}
    ]
  end
  
  def facts_collection # fact section
    [
      {number: "102", name: t("number_of_clients", scope: "facts.blocks")},
      {number: "170", name: t("finished_projects", scope: "facts.blocks")},
      {number: "8750", name: t("days_at_work", scope: "facts.blocks")},
      {number: "10<sup>6</sup>", name: t("lines_of_code", scope: "facts.blocks")},
      {number: "9204", name: t("drank_coffee_pots", scope: "facts.blocks")}
    ]
  end
  
  def projects_technologies_collection # selected projects section
    [
      {
        name: "mgs",
        layers: 3,
        technologies: [
          "Ruby on Rails",
          "HTML5",
          "WebGL",
          "Mandrill",
          "Backbone.js",
          "PayPal"
        ]
      },
      {
        name: "taxi5",
        layers: 4,
        technologies: [
          "Ruby on Rails",
          "HTML5",
          "Java",
          "Objective-C",
          ".NET",
          "WebSocket",
          "NodeJS",
          "Redis",
          "Android",
          "iOS",
          "API"
        ]
      },
      {
        name: "mood",
        layers: 3,
        technologies: [
          "Ruby on Rails",
          "Streaming",
          "REST API",
          "Backbone.js"
        ]
      },
      {
        name: "flicolo",
        layers: 5,
        technologies: [
          "Ruby on Rails",
          "HTML5",
          "Backbone.js",
          "OAuth 2",
          "ElasticSearch",
          "RWD", 
          "Android",
          "Redis"
        ]
      }
    ]
  end


end
