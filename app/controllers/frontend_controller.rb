class FrontendController < ApplicationController
  before_action :fetch_rss
  before_action :set_locale
  
  def index
    @contact_request = ContactRequest.new

    if params[:locale]
      if available_languages.include?(params[:locale])
        session[:locale] = params[:locale].to_sym
      else
        get_locale
      end
    end
    I18n.locale = session[:locale] if session[:locale].present?
  end

  def get_locale
    redirect_to "/#{locale}"
  end

  def contact_request
    @contact_request = ContactRequest.new contact_request_params
    #render text: @contact_request.service_names.inspect
    #return
    if @contact_request.save
      FrontendMailer.hire_us_form("mariusz.zak@rst-it.com", "system@kmpit.com", @contact_request).deliver
      flash[:notice] = I18n.t "form_success", scope: "messages"
      redirect_to action: :index
    else
      flash.now[:alert] = I18n.t "form_error", scope: "messages"
      render action: :index
    end
  end
  
  private
    def contact_request_params
      params.require(:contact_request).permit(:name, :telephone, :email, :description)
    end
    
    def extract_locale_from_accept_language_header
      request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first rescue "en"
    end
    
    def fetch_rss
      require 'rss'
      rss = RSS::Parser.parse(open('http://howwedoapps.com/articles.rss').read, false)
      @latest_post = rss.items.first
    rescue
      nil
    end

    def set_locale
      I18n.locale = session[:locale] || extract_locale_from_accept_language_header
      I18n.locale = :en unless available_languages.include?(locale.to_s)
      session[:locale] = locale
    end

    def available_languages
      %w{en de pl}
    end
end
