# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'lift-off'
set :repo_url, 'git@bitbucket.org:kmpgroup/lift-off.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/home/deploy/lift-off'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/unicorn/production.rb}

# Default value for linked_dirs is []
set :linked_dirs, %w{log tmp/pids tmp/sessions tmp/cache tmp/sockets }

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :rvm_type, :system                     # Defaults to: :auto
set :rvm_ruby_version, '2.1.1'      # Defaults to: 'default'

namespace :deploy do

  desc 'Restart application'
  task :restart do
    invoke 'unicorn:restart'
  end

  after :publishing, :restart

end

require './config/boot'
require 'airbrake/capistrano3'
