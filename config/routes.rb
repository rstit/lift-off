Kmpit::Application.routes.draw do
  scope '/:locale' do
    root 'frontend#index'
    devise_for :users
    
    post :contact_requests, path: "contact-request", controller: :frontend, action: :contact_request
  end

  get '/' => 'frontend#get_locale'
end
