class CreateContactRequests < ActiveRecord::Migration
  def change
    create_table :contact_requests do |t|
      t.string :name
      t.string :telephone
      t.string :email
      t.references :service, index: true
      t.timestamps
    end
  end
end
