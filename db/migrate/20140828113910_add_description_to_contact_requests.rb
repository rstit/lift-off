class AddDescriptionToContactRequests < ActiveRecord::Migration
  def change
    add_column :contact_requests, :description, :string, limit: 240
  end
end
