class ChangeServicesToHasMany < ActiveRecord::Migration
  def up
  	remove_column :contact_requests, :service_id
  	add_column :contact_requests, :services, :text
  end

  def down
  	add_column :contact_requests, :service_id, :integer
  	remove_column :contact_requests, :services
  end
end
