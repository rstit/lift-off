require 'spec_helper'

describe ContactRequest do

  describe 'validations' do
    subject { FactoryGirl.build(:contact_request)}

    it "is invalid without name" do
      subject.name = nil
      subject.valid?
      expect(subject.errors[:name]).to be_present
    end
    it "is invalid without telephone" do
      subject.telephone = nil
      subject.valid?
      expect(subject.errors[:telephone]).to be_present
    end
    it "is invalid without email" do
      subject.email = nil
      subject.valid?
      expect(subject.errors[:email]).to be_present
    end
    it "is invalid without description" do
      subject.description = nil
      subject.valid?
      expect(subject.errors[:description]).to be_present
    end
    it "is invalid with description longer than 240 characters" do
      subject.description = "a" * 241
      subject.valid?
      expect(subject.errors[:description]).to be_present
    end
  end
end
