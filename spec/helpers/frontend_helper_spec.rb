require "spec_helper"

describe FrontendHelper do
  describe "#display_base_errors" do

    let(:resource) { ContactRequest.new }

    context "resource has errors" do
      it "returns html containing errors from errors[:base]" do
        resource.errors[:base] << 'First error'
        resource.errors[:base] << 'Second error'
        html = helper.display_base_errors resource
        doc = Nokogiri::HTML(html)
        expect(doc.at('p:contains("First error")')).to be_present
        expect(doc.at('p:contains("Second error")')).to be_present
      end
    end

    context "resource has not errors" do
      it "returns empty string" do
        expect(helper.display_base_errors(resource)).to eq ''
      end
    end
  end

  describe "#section_heading" do
    it "returns html of translated header" do
      I18n.locale = :en
      html = helper.section_heading "hire_us"
      doc = Nokogiri::HTML(html)
      expect(doc.at('h2:contains("Hire us")')).to be_present
    end
  end

  describe "#parse_to_img_url" do
    it "returns given text downcased with replaced spaces to dashes" do
      expect(helper.parse_to_img_url("Lorem Ipsum DOLOR")).to eq 'lorem-ipsum-dolor'
    end
  end
end
