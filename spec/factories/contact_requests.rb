FactoryGirl.define do
  factory :contact_request do
    name { Faker::Name.name }
    telephone { Faker::PhoneNumber.phone_number }
    email { Faker::Internet.email }
    description { Faker::Lorem.sentence }
  end
end
