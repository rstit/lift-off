require "spec_helper"

describe FrontendMailer do
  describe '#hire_us_form' do
    before(:each) do
      I18n.locale = :de
    end

    let(:contact_request) { FactoryGirl.build(:contact_request) }
    let(:mail) { FrontendMailer.hire_us_form('to@email.com', 'from@email.com', contact_request) }
 
    it 'renders the subject' do
      expect(mail.subject).to eql('KMPIT - Hire Us')
    end
 
    it 'renders the receiver email' do
      expect(mail.to).to eql(['to@email.com'])
    end
 
    it 'renders the sender email' do
      expect(mail.from).to eql(['from@email.com'])
    end
 
    describe 'body of email' do
      it 'contains name' do
        expect(mail.body.encoded).to match(contact_request.name)
      end

      it 'contains telephone' do
        expect(mail.body.encoded).to match(Regexp.escape(contact_request.telephone))
      end

      it 'contains email' do
        expect(mail.body.encoded).to match(contact_request.email)
      end

      it 'contains description' do
        expect(mail.body.encoded).to match(contact_request.description)
      end
    end
  end
end
