require 'spec_helper'

describe FrontendController do
  describe 'GET #index' do
    context 'with params[:locale]' do
      context 'contained in white list' do
        it 'sets given session[:locale]' do
          get :index, locale: :de
          expect(session[:locale]).to eq :de
        end
      end
      context 'not contained in white list' do
        it 'redirects to previously setted locale' do
          get :index, locale: :something
          expect(response).to redirect_to root_path(session[:locale])
        end
      end
      it 'renders the :index template' do
        get :index, locale: :de
        expect(response).to render_template :index
      end
    end

    context 'without params[:locale]' do
      it 'redirects to url with locale' do
        session[:locale] = :de
        get :get_locale
        expect(response).to redirect_to root_path(session[:locale])
      end
    end
  end

  describe 'POST #contact_requests' do
    context 'with valid fields' do
      before(:each) do
        post :contact_request, locale: :en, contact_request: FactoryGirl.attributes_for(:contact_request)
      end

      after(:each) do
        ActionMailer::Base.deliveries.clear
      end

      it 'sends email to administrators' do
        expect(ActionMailer::Base.deliveries.count).to eq 1
      end
      it 'shows flash notice' do 
        expect(flash[:notice]).to be_present
      end

      it 'renders the frontend_mailer/hire_us_form template' do
        expect(response).to render_template 'frontend_mailer/hire_us_form'
      end
    end

    context 'with invalid fields' do
      before(:each) do
        post :contact_request, locale: :en, contact_request: { name: 'something' }
      end

      it 'shows flash alert' do
        expect(flash[:alert]).to be_present
      end
      it 'renders the index template' do
        expect(response).to render_template :index
      end
    end
  end


end
